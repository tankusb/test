USE [ProgramData]
GO
/****** Object:  Table [dbo].[luAcademicYear]    Script Date: 8/15/2019 9:02:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[luAcademicYear](
	[AYSequentialOrder] [int] NOT NULL,
	[AcademicYear] [nvarchar](50) NULL
) ON [PRIMARY]
GO
